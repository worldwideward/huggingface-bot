FROM python:3.12

RUN pip install \
	requests \
	pylint

WORKDIR /app

COPY ./hf_inference_api.py api.py

RUN pylint --fail-under 9.30 --recursive y -f colorized .

USER nobody

CMD ["python", "/app/api.py"]
