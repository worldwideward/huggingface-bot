#!/bin/bash

## create a .env file, and add 'export HF_API_TOKEN=[your free huggingface api token]'
## then execute 'source .env' in your terminal (verify it worked with 'env | grep HF_API_TOKEN')

## Exit the bot with ctrl+c

docker_image_name="huggingface-bot"
docker_image=$(docker image ls $docker_image_name --format json | jq -r '.Repository')

## Check if the api token is properly configured
#
if [ -z "$HF_API_TOKEN" ]; then

	token_file=".env"
	if [ ! -f $token_file ]; then

		echo "[ERROR] $token_file file not found"
		exit 1
	fi
	source $token_file	

	if [ -z "$HF_API_TOKEN" ]; then

		echo "[ERROR] $token_file doens't properly set HF_API_TOKEN variable"
		exit 1
	fi
fi

## Build the docker image if it doesn't exist (it doesn't rebuild)
#
if [ -z "$docker_image" ]; then

	docker build . -t $docker_image_name
fi

## If everything checks out, run the bot

docker run --rm -ti -e HF_API_TOKEN=$HF_API_TOKEN $docker_image_name
