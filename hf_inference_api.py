#!/usr/bin/env python
'''This module spawns a simple Huggingface chat client'''

import os
import json
import requests

class ChatBot():
    '''This class implements the Chatbot'''

    def __init__(self, model):

        self.name = "Huggingface AI Bot"

        self.api_url = f'https://api-inference.huggingface.co/models/{model}'
        self.api_token = os.environ['HF_API_TOKEN']
        self.max_wait_for_answer = 10
        self.max_new_tokens = 250
        self.model = model

    def query(self, payload):
        ''' Execute a message query '''

        headers  = {"Authorization": f"Bearer {self.api_token}"}
        response = requests.post(self.api_url, headers=headers, json=payload, timeout=60)

        return response.json()

    def format_response(self, data):
        ''' Format query response '''

        try:
            text = json.dumps(data[0]['generated_text'], sort_keys=True, indent=4)

        except KeyError:
            text = json.dumps(data, sort_keys=True, indent=4)

        formatted_text = text.replace('\\n', '\n').replace('\\t', '\t')

        return formatted_text

    def construct_payload(self, question):
        ''' Construct a query payload '''

        payload = {
	        "inputs": str(question),
	        "parameters": {
	            "max_new_tokens": self.max_new_tokens,
	            "max_time": self.max_wait_for_answer,
	            "return_full_text": False,
	            "do_sample": False,
	        }
	    }

        return payload

def select_model():
    ''' Select a NLP Model '''

    models = (
        'h2oai/h2o-danube-1.8b-chat', 
        'gpt2',
        'deepset/roberta-base-squad2'
    )

    for model in models:

        print(f'{models.index(model)} - {model}')

    selection = input("[Prompt] Enter the index (number) of the model you would like to use: ")
    selected_model = models[int(selection)]

    return str(selected_model)

def select_question():
    ''' Select a question '''

    questions = (
        'Create a minimal Dockerfile for a Python flask app',
        'Check if my Dockerfile has configuration mistakes',
        'Write a test for this function: def hello_world(): print("hello")',
    )

    for question in questions:

        print(f'{questions.index(question)} - {question}')

    selection = input("[Prompt] Enter the index (number) of the question you would like to ask: ")
    selected_question = questions[int(selection)]

    return str(selected_question)

def main():
    ''' Main function '''


    print('[INFO] Select a NLP Model from the list to use during the session')
    model = select_model()
    print(f'[DEBUG] You chose to use model: {model}')
    bot = ChatBot(model)
    print(f'[DEBUG] Initialized chatbot {bot.name}')
    print(f'[DEBUG] Max answer wait {bot.max_wait_for_answer}')
    print(f'[DEBUG] Max tokens {bot.max_new_tokens}')
    print('[INFO] Starting session')

    while True:

        ### You can set fixed questions instead of free talk ###
        question = input("[Message] ")
        #question = select_question()

        payload = bot.construct_payload(question)
        data = bot.query(payload)
        print(bot.format_response(data))
        print('==== ==== ==== ====')

if __name__ == "__main__":

    main()
