# Huggingface.co Inference API bot

This simple bot relays questions and answers between you and the Huggingface Inference API.

You can select a different model to interact with each session.

Docs on [huggingface.co](https://huggingface.co/docs/api-inference/index)

## Quick start

1. Create an account at Huggingface.co and create an access token with READ permissions at https://huggingface.co/settings/tokens

2. Create a `.env` file and add `export HF_API_TOKEN=xxxxx`

3. Run `./docker.sh`

If everything goes right, you will get an interactive chat session:

```bash
0 - h2oai/h2o-danube-1.8b-chat
1 - gpt2
2 - deepset/roberta-base-squad2
Enter the index (number) of the model you would like to use: 0
[DEBUG] Using model: h2oai/h2o-danube-1.8b-chat
Message: Create a multi stage Dockerfile for a golang project
".

1. Create a new directory for the project.
...
```
